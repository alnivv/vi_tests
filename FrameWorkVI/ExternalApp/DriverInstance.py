from selenium import webdriver

class DriverInstance:

    driver = None
    def get_Driver_Instance(self):
        options = webdriver.ChromeOptions()
        options.add_argument("--disable-notifications")
        self.driver = webdriver.Remote(
                    'http://127.0.0.1:4444/wd/hub',
                     options.to_capabilities()
                )
        self.driver.maximize_window()
        return self.driver

    def get_driver(self):
        if self.driver is None:
            self.get_Driver_Instance()
        return self.driver

    def stop_test(self):
        if self.driver is not None:
            self.driver.quit()
