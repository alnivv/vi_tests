import time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException, StaleElementReferenceException
from selenium.webdriver.support.wait import WebDriverWait

from FrameWorkVI.WEB.AnyPage import AnyPage


class waitingFW(AnyPage):

    def __init__(self, manager):
        super().__init__(manager)

    # ищем элемент "time_element_wait" секунд
    def waiting_for_the_element(self, locator):
        try:
            WebDriverWait(self.get_driver(), self.time_element_wait).until(EC.visibility_of_element_located((By.XPATH, locator)))
        except TimeoutException as e:
            pass

    # ищем элемент "time_element_wait" секунд
    def waiting_for_the_element_to_be_clickable(self, locator):
        try:
            WebDriverWait(self.get_driver(), self.time_element_wait).until(EC.element_to_be_clickable((By.XPATH, locator)))
        except TimeoutException as e:
            pass

    def check_page_to_load(self, locator):
        index = 0
        while True:
            try:
                if self.time_element_wait < index:
                    return False
                self.get_driver().find_element_by_xpath(locator)
                return True
            except NoSuchElementException:
                return False
            except StaleElementReferenceException:
                time.sleep(0.2)
                index = index + 0.2

