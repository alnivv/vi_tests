import random
from FrameWorkVI.BaseFW import BaseFW

title_text = '//meta[@name = "description"]/preceding-sibling::title'

header = '//header[@id = "header"]'
region_list = '//span[@id="selectRegionsList"]'
region_selector = region_list + '//preceding::a'
region_list_form = '//form[@id="check"]'
small_delivery_regions = '//div[contains(@class," deliveryCourierConteiner ")]/a[@class=" c-black linkRedHover ' \
                         'region_check"] '

catalog_search_box = '//div[@id="catalogSearchBox"]'
actions = catalog_search_box + '//div[contains(@class,"dspl_ib valign-m action")]'
actions_active_bt = '//div[contains(@class,"bt-content")]'
actions_list = actions_active_bt + '//ul'
actions_list_sale = actions_list + '/li/a[contains(text(),"Распродажа")]'

catalog = '//section[@data-behavior="catalog"]'
catalog_items = '//div[@data-behavior="catalog-list-items"]'
tool = '//section[@data-item-id="1"]'
tool_link = tool + '//div[contains(@class,"group ")]/a'

contacts_link = '//a[contains(@href,"/contacts/")]'
contacts_button = header + contacts_link


class AnyPage(BaseFW):

    def get_title_text(self):
        text = self.get_tag_attribute_inner_html(title_text)
        return text

    def click_region_selector(self):
        self.click_by_xpath(region_selector)
        # Дожидаемся загрузки формы выбора региона
        self.manager.waiting.waiting_for_the_element(region_list_form)
        return self

    def click_contacts_button(self):
        self.click_by_xpath(contacts_button)
        # Дожидаемся перехода на страницу контактов
        self.manager.contacts_page.waiting_for_page_to_load()
        return self.manager.contacts_page

    def move_to_actions(self):
        self.move_to_element(actions)
        # Дожидаемся загрузки списка акций
        self.manager.waiting.waiting_for_the_element(actions_list)
        return self

    def click_sale_action(self):
        self.click_by_xpath(actions_list_sale)
        # Дожидаемся перехода на страницу распродажи
        self.manager.rasprodazha.waiting_for_page_to_load()
        return self.manager.rasprodazha

    def select_region_random(self):
        self.click_by_xpath(region_selector)
        # Дожидаемся загрузки формы выбора региона
        self.manager.waiting.waiting_for_the_element(region_list_form)
        # Находим все регионы, не выделенные полужирным шрифтом, куда возможна доставка
        regions_list = self.get_driver().find_elements_by_xpath(small_delivery_regions)
        regions_count = len(regions_list)
        # Берём случайное число из полученного количества регионов
        region_number = random.randint(1, regions_count)
        # Кликаем по случайному региону
        self.click_by_xpath('(' + small_delivery_regions + ')' + f'[{region_number}]')
        return self

    def move_to_catalogue(self):
        self.move_to_element(catalog)
        # Дожидаемся загрузки элементов каталога
        self.manager.waiting.waiting_for_the_element(catalog_items)
        return self

    def move_to_tool(self):
        self.move_to_element(tool)
        # Дожидаемся загрузки раздела инструментов и возможности кликнуть по разделу
        self.manager.waiting.waiting_for_the_element_to_be_clickable(tool_link)
        self.move_to_element(tool_link)
        self.click_by_xpath(tool_link)
        self.manager.instrument.waiting_for_page_to_load()
        return self.manager.instrument
