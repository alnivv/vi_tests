from FrameWorkVI.WEB.AnyPage import AnyPage


class ItemPage(AnyPage):

    head = '//h1[@id="card-h1-reload-new"]'
    delivery_block = '//div[@id="delivery-block"]'
    sale_block = '//div[contains(@class,"retail-sale__body")]'
    add_to_cart_sale_button = sale_block + '//a[@data-action-id="retail_sale"]'

    header_after_adding = '//header/strong[contains(text(),"Товар добавлен в корзину")]'

    best_price = '//span[contains(text(),"Лучшая цена")]'
    able_to_adding = 'div[contains(@class,"buttons ns")]'
    best_price_and_able_to_adding = best_price + '//..//..//..//..//div' + able_to_adding

    def waiting_for_page_to_load(self):
        self.manager.waiting.check_page_to_load(self.head)

    def click_add_to_cart_sale_item(self):
        self.click_by_xpath(self.add_to_cart_sale_button)
        return self

    def waiting_for_header_after_adding(self):
        self.manager.waiting.waiting_for_the_element(self.header_after_adding)

    def click_adding_best_price_item(self):
        self.click_by_xpath(self.best_price_and_able_to_adding)
        # Дожидаемся перехода на страницу товара
        self.manager.item_page.waiting_for_page_to_load()
        return self.manager.item_page


