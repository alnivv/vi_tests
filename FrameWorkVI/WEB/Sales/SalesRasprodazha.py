from FrameWorkVI.WEB.AnyPage import AnyPage


class Rasprodazha(AnyPage):

    goods = '//div[@id="goods"]'

    list_item = '//div[contains(@class,"tile-box product")]'
    list_item_name = '//div[contains(@class,"product-name")]'
    first_item = list_item + '[1]'
    first_item_name = first_item + list_item_name
    pickup_item = list_item_name + '//..//span[contains(text(),"Только самовывоз")]'
    able_to_adding_item = '//a[@data-action-id="retail_sale"]//..//..' + list_item_name

    def waiting_for_page_to_load(self):
        self.manager.waiting.check_page_to_load(self.goods)

    def click_first_item_page(self):
        self.click_by_xpath(self.first_item_name)
        # Дожидаемся перехода на страницу товара
        self.manager.item_page.waiting_for_page_to_load()
        return self.manager.item_page

    def click_pickup_item(self):
        self.click_by_xpath(self.pickup_item)
        # Дожидаемся перехода на страницу товара
        self.manager.item_page.waiting_for_page_to_load()
        return self.manager.item_page

    def click_adding_item(self):
        self.click_by_xpath(self.able_to_adding_item)
        # Дожидаемся перехода на страницу товара
        self.manager.item_page.waiting_for_page_to_load()
        return self.manager.item_page



