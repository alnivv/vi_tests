from FrameWorkVI.WEB.AnyPage import AnyPage


class InstrumentGravery(AnyPage):

    goods = '//div[@id="goods"]'
    big_type_block = '//div[contains(@class,"block -big")]'
    gravery_akkum_link = big_type_block + '/a[contains(@href,"/instrument/gravery/akkumulyatornye/")]'

    def waiting_for_page_to_load(self):
        self.manager.waiting.check_page_to_load(self.goods)

    def click_gravery_akkum(self):
        self.click_by_xpath(self.gravery_akkum_link)
        # Дожидаемся перехода на страницу товара
        self.manager.instrument_gravery_akkum.waiting_for_page_to_load()
        return self.manager.instrument_gravery_akkum
