from FrameWorkVI.WEB.AnyPage import AnyPage


class InstrumentGraveryAkkumulyatornye(AnyPage):
    heading = '//div[contains(@class,"content")]/h1[contains(text(),"Аккум")]'
    goods = '//div[@id="goods"]'

    best_price = '//span[contains(text(),"Лучшая цена")]'
    able_to_adding = '//div[contains(@class,"buttons ns")]'
    best_price_and_able_to_adding = best_price + '//..//..//..//..//div' + able_to_adding

    def waiting_for_page_to_load(self):
        self.manager.waiting.check_page_to_load(self.heading)

    def click_adding_best_price_item(self):
        self.click_by_xpath(self.best_price_and_able_to_adding)
        # Дожидаемся перехода на страницу товара
        self.manager.item_page.waiting_for_page_to_load()
        return self.manager.item_page



