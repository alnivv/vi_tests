from FrameWorkVI.WEB.AnyPage import AnyPage

class Instrument(AnyPage):

    heading = '//div[contains(@class,"content")]/h1'
    name_block = '//div[contains(@class,"name")]'
    gravery_link = name_block + '/a[@href="/instrument/gravery/"]'

    def waiting_for_page_to_load(self):
        self.manager.waiting.check_page_to_load(self.heading)

    def click_gravery(self):
        self.click_by_xpath(self.gravery_link)
        # Дожидаемся перехода на страницу товара
        self.manager.instrument_gravery.waiting_for_page_to_load()
        return self.manager.instrument_gravery





