from FrameWorkVI.WEB.AnyPage import AnyPage


class ContactsPage(AnyPage):

    contact_page = '//div[contains(@class,"contact_page")]'

    hr_department_block = '//tr/td[contains(text(),"Отдел подбора персонала")]'
    hr_department_phone_number = hr_department_block + '//..//td/div[contains(text(),"+7")]'

    def waiting_for_page_to_load(self):
        self.manager.waiting.check_page_to_load(self.contact_page)

    def move_to_hr_department(self):
        self.move_to_element(self.hr_department_block)
        return self

    def get_hr_number(self):
        hr_number = self.get_tag_text(self.hr_department_phone_number)
        return hr_number
