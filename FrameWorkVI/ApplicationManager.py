from FrameWorkVI.WEB.AnyPage import AnyPage
from FrameWorkVI.WEB.ContactsPage import ContactsPage
from FrameWorkVI.WEB.Instrument.Gravery.InstrumentGravery import InstrumentGravery
from FrameWorkVI.WEB.Instrument.Gravery.InstrumentGraveryAkkumulyatornye import InstrumentGraveryAkkumulyatornye
from FrameWorkVI.WEB.Instrument.Instrument import Instrument
from FrameWorkVI.WEB.MainPage import MainPage
from FrameWorkVI.WEB.Sales.SalesRasprodazha import Rasprodazha
from FrameWorkVI.WEB.ItemPage import ItemPage
from FrameWorkVI.WaitingFW import waitingFW
from FrameWorkVI.ExternalApp.DriverInstance import DriverInstance


class Application_Manager:
    driver = None

    def __init__(self):

        self.driver_instance = DriverInstance()
        self.waiting = waitingFW(self)
        self.any_page = AnyPage(self)
        self.main_page = MainPage(self)
        self.rasprodazha = Rasprodazha(self)
        self.item_page = ItemPage(self)
        self.instrument = Instrument(self)
        self.instrument_gravery = InstrumentGravery(self)
        self.instrument_gravery_akkum = InstrumentGraveryAkkumulyatornye(self)
        self.contacts_page = ContactsPage(self)




