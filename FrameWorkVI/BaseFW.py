from selenium.webdriver.common.action_chains import ActionChains


class BaseFW(object):
    time_element_wait = 30
    driver = None

    def __init__(self, application_manager):
        self.manager = application_manager

    def get_driver(self):
        if self.driver is None:
            self.driver = self.manager.driver_instance.get_driver()
        return self.driver

    def open_main_page(self, main_page):
        title = self.get_driver().title
        if main_page not in title:
            self.get_driver().get(main_page)

    def click_by_xpath(self, locator):
        self.get_driver().find_element_by_xpath(locator).click()

    def get_tag_text(self, locator):
        text = self.get_driver().find_element_by_xpath(locator).text
        return text

    def get_tag_attribute(self, locator, attribute_name):
        text = self.get_driver().find_element_by_xpath(locator).get_attribute(attribute_name)
        return text

    def get_tag_attribute_inner_html(self, locator):
        text = self.get_driver().find_element_by_xpath(locator).get_attribute('innerHTML')
        return text

    def move_to_element(self, locator):
        actions = ActionChains(self.get_driver())
        element = self.get_driver().find_element_by_xpath(locator)
        actions.move_to_element(element)
        actions.perform()















