from FrameWorkVI.ApplicationManager import Application_Manager


class ViTestBase:
    APP = Application_Manager()
    main_page = 'https://www.vseinstrumenti.ru/'

    def setup_class(self):
        self.APP.any_page.open_main_page(self.main_page)

    def setup_method(self):
        pass

    def teardown_method(self):
        pass

    def teardown_class(self):
        self.APP.driver_instance.stop_test()



