from TestsVI.Tests.ViTestBase import ViTestBase
import time


class TestTaskVi(ViTestBase):
    # №1
    # https://www.vseinstrumenti.ru/
    # Автотест взятия заголовка страницы (то, что в заголовке вкладки/окна)
    def test_get_title_text(self):
        # Получаем заголовок для главной страницы сайта (Содержится на вкладке браузера)
        title_text = self.APP.main_page.get_title_main()
        assert 'Интернет-магазин ВсеИнструменты.ру - электроинструмент и оборудование: климатическое, садовое, ' \
               'клининговое, автогаражное' in title_text

    # №2
    # https://www.vseinstrumenti.ru/
    # Автотест нажатия на ссылку вызова формы выбора города
    def test_click_region_selector(self):
        # Нажимаем на ссылку вызова формы выбора города
        self.APP.any_page.click_region_selector()
        # Закоментированная задержка, чтобы успеть рассмотреть, как отработал тест
        # time.sleep(3)

    # №3
    # https://www.vseinstrumenti.ru/stanki/sverlilnye/kraton/kraton-stanok-sverlilnyj-dm-16-500-4-02-04-008-4-02-04-008/
    # (или любой другой товар из «распродажи»)
    # Написать Автотест нажатия кнопки "В корзину" - по распродажной цене
    def test_add_sale_item_to_cart(self):
        # Выбираем "Распродажа" в списке акций
        self.APP.any_page.move_to_actions()
        self.APP.any_page.click_sale_action()
        # Переходим на страницу первого подходящего товара со страницы распродажи (который можно добавить в корзину)
        self.APP.rasprodazha.click_adding_item()
        # Нажимаем "В корзину" в блоке "Распродажа в магазине"
        self.APP.item_page.click_add_to_cart_sale_item()
        # Убеждаемся, что появилась форма с надписью "Товар добавлен в корзину"
        self.APP.item_page.waiting_for_header_after_adding()
        # Закоментированная задержка, чтобы успеть рассмотреть, как отработал тест
        # time.sleep(3)

    # №4 https://www.vseinstrumenti.ru/
    # В хедере вызвать форму выбора города. Написать автотест для выбора случайного
    # региона, в котором возможна курьерская доставка (присутствует иконка грузовика) и который не выделен полужирным
    # шрифтом (не Москва, не Санкт-Петербург, ..., не Казань)
    def test_select_region_random(self):
        # Выбираем рандомный город, не выделенный полужирным, куда возможна доставка
        self.APP.any_page.select_region_random()
        # Закоментированная задержка, чтобы успеть рассмотреть, как отработал тест
        # time.sleep(3)

    # №5
    # https://www.vseinstrumenti.ru/instrument/gravery/akkumulyatornye/
    # Написать автотест нажатия кнопки "В корзину" для случайного товара с признаком "Лучшая цена"
    def test_add_best_price_item_to_cart(self):
        # Выбираем категорию "Инструмент" в каталоге
        self.APP.any_page.move_to_catalogue()
        # Переходим на страницу "Инструмент"
        self.APP.any_page.move_to_tool()
        # На странице "Инструмент" кликаем на категорию "Граверы"
        self.APP.instrument.click_gravery()
        # На странице "Граверы" кликаем на категорию "Аккумуляторные"
        self.APP.instrument_gravery.click_gravery_akkum()
        # Добавить в корзину товар с признаком "Лучшая цена"
        self.APP.instrument_gravery_akkum.click_adding_best_price_item()
        # Закоментированная задержка, чтобы успеть рассмотреть, как отработал тест
        # time.sleep(3)

    # №6
    # https://www.vseinstrumenti.ru/contacts/1.html
    # Для страницы "Контакты" написать автотест, который узнаёт телефон отдела подбора персонала
    def test_contacts_hr_number(self):
        # Нажимаем на кнопку "Контакты" в хедере
        self.APP.any_page.click_contacts_button()
        self.APP.contacts_page.move_to_hr_department()
        hr_phone_number = self.APP.contacts_page.get_hr_number()
        assert '+7 (495) 647-60-00' in hr_phone_number


























